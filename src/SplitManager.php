<?php
namespace TkachInc\Splits;
use TkachInc\Engine\Services\Helpers\ArrayHelper;

/**
 * Split Manager
 */
class SplitManager
{
	/**
	 * @var array Current user splits
	 */
	protected $userSplits;

	/**
	 * SplitManager constructor.
	 *
	 * @param array $userSplits
	 */
	public function __construct(Array $userSplits = [])
	{
		$this->userSplits = $userSplits;
	}

	/**
	 * Return split by name
	 * @param string $name
	 * @param int $defaultSplit
	 * @param bool $reset
	 * @return int Default return 1, and add new split in userSplits
	 */
	public function getSplit($name = 'base', $defaultSplit = 1, $reset = false)
	{
		if (!$reset && isset($this->userSplits[$name])) {
			$split = $this->userSplits[$name];
		} else {
			$this->userSplits[$name] = $defaultSplit;
			$split = $defaultSplit;
		}

		return $split;
	}

	/**
	 * Return new current splits (you must get and save this)
	 * @return array
	 */
	public function getUserSplits()
	{
		return $this->userSplits;
	}

	/**
	 * Check and correct user splits
	 * @return $this
	 */
	public function checkSplit($install = false)
	{
		$availableSplits = SplitConfig::getAll();
		$splits = [];
		foreach ($availableSplits as $name) {
			if ($name['enable'] === true) {
				if ($install === false || ($name['onlyInstall'] === true && $install === true)) {
					$splits[$name['_id']] = 1;
					if (!isset($this->userSplits[$name['_id']])) {
						$value = ArrayHelper::weightedIndex($name['values']);
						$this->getSplit($name['_id'], $value, true);
					} elseif (array_key_exists($this->userSplits[$name['_id']], $name['values']) === false) {
						$value = ArrayHelper::weightedIndex($name['values']);
						$this->getSplit($name['_id'], $value, true);
					}
				}
			}
		}
		foreach ($this->userSplits as $key => $value) {
			if (!isset($splits[$key])) {
				unset($this->userSplits[$key]);
			}
		}

		return $this;
	}
}