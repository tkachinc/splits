<?php
namespace TkachInc\Splits;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * Config Splits
 *
 * @property mixed values
 * @package Poker\UserService\Users\Model
 */
class SplitConfig extends ObjectModel
{
	protected static $_collection = 'split_config';
	protected static $_pk = '_id';
	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
	];
	protected static $_sort = ['_id' => 1];
	protected static $_fieldsDefault = [
		'_id'         => 'base',
		'values'      => [],
		'comments'    => '',
		'onlyInstall' => false,
		'enable'      => false,
	];

	protected static $_fieldsValidate = [
		'_id'         => self::TYPE_STRING,
		'values'      => self::TYPE_JSON,
		'comments'    => self::TYPE_STRING,
		'onlyInstall' => self::TYPE_BOOL,
		'enable'      => self::TYPE_BOOL,
	];

	protected static $_fieldsHint = [
		'_id'    => 'ID',
		'values' => 'Array split: chanse',
	];
	protected static $_hasPostfix = true;
}
